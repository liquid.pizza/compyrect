from dash import Dash, html, dcc, Input, Output
import plotly.express as px
import compyrect as cp
import math, os

app = Dash(
    __name__,
    external_stylesheets=['https://codepen.io/chriddyp/pen/bWLwgP.css'],
    )

DATA_DIR = "data"
DATA_ENDING = ".csv"

def get_data_files(root_dir=DATA_DIR, file_ending=DATA_ENDING):
    ret = []
    for root, _, files in os.walk(root_dir):
        for file in files:
            if file.endswith(file_ending):
                ret.append(f"{root}/{file}")

    return ret

def generate_layout():

    file_list = get_data_files()

    return html.Div(children=[
        html.H1(children='Welcome to your compyrect dash'),
        html.Div(children=[
            html.H2("Select an input file."),
            dcc.Dropdown(file_list, value=file_list[0], id="file-dropdown"),
        ]),
        html.Div(id="visualization"),
    ])

@app.callback(
    Output("visualization", "children"),
    Input("file-dropdown", "value")
)
def visualize_data_by_path(path):
    return visualize_data(cp.load_df(path))

def visualize_data(df):

    fig_by_partner = px.pie(df[df[cp.ABS_VALUE_LABEL].notna()], values=cp.ABS_VALUE_LABEL, names=cp.CLIENT_LABEL)
    fig_by_date_partner = px.bar(df[df[cp.CLIENT_LABEL].notna()], x=cp.DATE_LABEL, y=cp.VALUE_LABEL, color=cp.CLIENT_LABEL)

    fig_by_month = px.sunburst(df[df[cp.ABS_VALUE_LABEL].notna() & df[cp.CLIENT_LABEL].notna()], path=[cp.MONTH_LABEL, cp.WEEKDAY_LABEL, cp.CLIENT_LABEL], values=cp.ABS_VALUE_LABEL)
    fig_by_weekday = px.sunburst(df[df[cp.ABS_VALUE_LABEL].notna() & df[cp.CLIENT_LABEL].notna()], path=[cp.WEEKDAY_LABEL, cp.CLIENT_LABEL], values=cp.ABS_VALUE_LABEL)


    return html.Div(children=[
        html.Div(children=f"Overall sum from {df[cp.DATE_LABEL].min().date()} to {df[cp.DATE_LABEL].max().date()}:\n\t{math.ceil(df[cp.VALUE_LABEL].sum())}"),

        html.Div([
            html.Div([
                html.H2("Expanses"),
                dcc.Graph(
                    id='fig_by_partner',
                    figure=fig_by_partner
                )],
                className="four columns",
                ),
            html.Div([
                dcc.Graph(
                    id='fig_by_date_partner',
                    figure=fig_by_date_partner
                )],
                className="eight columns",
                ),
            ],
            className="row",
        ),
        html.Div([
            html.Div([
                dcc.Graph(
                    id='fig_by_month',
                    figure=fig_by_month
                )],
                className="four columns",
                ),
            html.Div([
                dcc.Graph(
                    id='fig_by_weekday',
                    figure=fig_by_weekday
                )],
                className="four columns",
            )],
            className="row",
        ),
    ])
    

if __name__ == "__main__":
    app.layout = generate_layout()
    app.run_server(host="0.0.0.0", debug=True)