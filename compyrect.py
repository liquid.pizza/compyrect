#! /usr/bin/env python3

import pandas as pd

# Cols in the csv
DATE_LABEL = "Buchungstag"
DESC_LABEL = "Buchungstext"
VALUE_LABEL = "Umsatz in EUR"
PROCESS_LABEL = "Vorgang"

# My cols
ABS_VALUE_LABEL = "Abs value"
CLIENT_LABEL = "Client"
WEEKDAY_LABEL = "Day of the week"
MONTH_LABEL = "Month"


PROCESS_CASH_LABEL = "Bar"
PROCESS_PAYMENT_LABEL = "Auszahlung"

WEEKDAY_NAMES = [
    "Mon",
    "Tue",
    "Wen",
    "Thu",
    "Fri",
    "Sat",
    "Sun",
]

CSV_PATH = "data/sample.csv"

def load_df(path = CSV_PATH):
    # read data
    # TODO: modify for real input
    df = pd.read_csv(
        path,
        sep=";",
        skiprows=4,
        skipfooter=4,
        engine="python",
        encoding = "ISO-8859-1",
        usecols=[DATE_LABEL, DESC_LABEL, VALUE_LABEL, PROCESS_LABEL],
        )

    # convert dates to datetime objects
    df[DATE_LABEL] = pd.to_datetime(df[DATE_LABEL], errors="coerce", format="%d.%m.%Y")

    def extractValue(l):
        '''
        Values are given as string.
        
        A . marks the thounds.
        A , seperates the Euro from the Cent value. 
        '''
        # remove point after thounds
        ret = l.replace(".", "")
        # replace the , with .
        ret = ret.replace(",", ".")
        return float(ret)

    # convert the value to a float
    df[VALUE_LABEL] = df[VALUE_LABEL].apply(extractValue)

    # compute absolute values for expanses
    df[ABS_VALUE_LABEL] = df[df[VALUE_LABEL] < 0][VALUE_LABEL].abs()

    # extract the partner for all payments
    df[CLIENT_LABEL] = df[~(df[PROCESS_LABEL].str.contains(PROCESS_CASH_LABEL) | df[PROCESS_LABEL].str.contains(PROCESS_PAYMENT_LABEL))][DESC_LABEL].str.split(": ").apply(lambda a: a[1])
    # "clean" the partner name
    df[CLIENT_LABEL] = df[CLIENT_LABEL].str.replace(" Buchungstext", "")
    df[CLIENT_LABEL] = df[CLIENT_LABEL].str.replace("Kto/IBAN", "")

    df[WEEKDAY_LABEL] = df[DATE_LABEL].dt.day_name()
    df[MONTH_LABEL] = df[DATE_LABEL].dt.month_name()

    return df

    # split data by month
    monthly_df = [df[df[DATE_LABEL].dt.month == m] for m in range(1,13)]

    # sum up each month
    monthly_sum = pd.Series(
        [m[VALUE_LABEL].sum() for m in monthly_df],
        index=range(1,13)
        )

    # filter by day of the week
    weekday_df = [df[df[DATE_LABEL].dt.dayofweek == d] for d in range(7)]

    weekday_norm_df = [df[df[VALUE_LABEL] <= 0][df[DATE_LABEL].dt.dayofweek == d] for d in range(7)]

    # sum up each week day
    weekday_sum = pd.Series(
        [d[VALUE_LABEL].sum() for d in weekday_df],
        index=WEEKDAY_NAMES
    )

    weekday_norm_sum = pd.Series(
        [d[VALUE_LABEL].sum() for d in weekday_norm_df],
        index=WEEKDAY_NAMES,
        name="Payments per day of the week",
    )

    # more detailed PayPal stats
    # print(f"PayPal-Text:\n{df[df[DESC_LABEL].str.contains('PayPal')][DESC_LABEL][216]}")
    df_paypal = df[df[DESC_LABEL].str.contains('PayPal')]
    # print(f"df_paypal:\n{df_paypal}")
    df_paypal["PayPal Betreff"] = df_paypal[DESC_LABEL].str.extract(r".* \. (.*),.*")
    # print(f"df_paypal:\n{df_paypal['PayPal Betreff']}")
    print(f"PayPal stats:\n\n{df_paypal.groupby('PayPal Betreff').sum().nsmallest(20, VALUE_LABEL)}\n\n")

    # print(f"nsmallest: {df.groupby(PARTNER_LABEL).sum().nsmallest(20, VALUE_LABEL)}")
    # print(f"nlargest: {df.groupby(PARTNER_LABEL).sum().nlargest(20, VALUE_LABEL)}")
    # print(f"overall sum from {df[DATE_LABEL].min().date()} to {df[DATE_LABEL].max().date()}:\n\t{df[VALUE_LABEL].sum()}")

if __name__ == "__main__":
    import fire
    fire.Fire(load_df)